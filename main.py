from forexbot import Robotics


robotics = Robotics()
robotics.loadData(datasheet='EURUSD_H1',
				all_names=["Date","Open","Max","Min","Close","Volume"],
				names=["Open","Max","Min","Close","Volume"],
				separator=',',
				)
				#applyfloat
				#header
				
robotics.generateXY(test_cases_count = 500,
					test_cases_multiplier = 2,
					sight_range = 50,
					bonus_sight_range = 3,
					target_name = "Close",
					data_divider = 1,
					forced_divisibility_by = 100,
					print_example=True,
					validate=False)
					#ETA_every
					
robotics.createModel(fromModel = "forexmodel-84.4%-2020-11-17-1605640589.329351",
					size_divider = 1,
					size_multiplier = 1)
					#fromModel 
					#NOTE: size_divider(max):11 
					
while(input("Train again?")==""):
	robotics.train(epochs = 20,
					steps = 0,
					max_range=200,
					steps_force_custom=False,
					early_stop=False)
					#steps_force_custom
					
	robotics.test(print_tests = False,
					print_every=50,
					columns_count=10,
					jacked_conversion=0) #0-off
					
	robotics.save()


        


