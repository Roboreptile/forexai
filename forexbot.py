import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import tensorflow as tf
from tensorflow.keras.layers import	Dense, InputLayer, BatchNormalization

from time import time, sleep
from datetime import datetime
from random import randint, sample
from sys import exit
import numpy as np
import pandas as pd
import math


class Robotics:
	def __init__(self):
		self.model = 0
		
		self.sight_range = 0 ##
		self.input_size = 0 ##
		self.bonus_sight_range = 0 ##
		self.forced_divisibility_by = 1
		
		self.X = 0 ##
		self.Y = 0 ##
		
		self.names = [] ##
		self.target_name = "" ##
		self.datasheet = "" ##
		self.test_cases_count = 0 ##
		self.test_cases_multiplier = 0 ##
		
		self.dataDictionary = {} ##
		self.data_count = 0 ##
		self.full_data_count = 0 ##
		self.correct = 0 ##
		
		print(" ========================== ")
		print("       FOREXBOT v1.0        ")
		print(" ========================== ")
	
	def loadData(self, datasheet:str, all_names, names, separator:str, applyfloat=[], header = None):
		"""datasheet (str) - name of the datasheet in data folder
		all_names (str[]) - array with column names
		names (str[]) - array with columns you wish to use
		separator (str) - separator of the csv file (;/,/ect)
		applyfloat (int[]) - array that applies float() func to corresponding {name from names} column if set to 1, default = [] (*all apply float*)
		header (int) - header rows in the csv file, default: None
		"""
		time_start = time()
		data = pd.read_csv("data/"+datasheet+".csv",header=header,names=all_names, sep=separator)
		self.datasheet = datasheet
		self.data_count = len(data)
		self.full_data_count = self.data_count
		self.input_size = len(names)
		self.names = names
		filterIF = lambda value,filter: float(value) if filter!=0 else value
		for idx,name in enumerate(names):
			filter = 1
			try: filter = applyfloat[idx]
			except: pass
			self.dataDictionary[name] = [filterIF(v,filter) for v in data[name]]
		
		print("Datasheet: '" + datasheet +".csv' loaded to memory in "+str(round(time()-time_start,3))+" ms")
		print("Columns: " + ",".join(names))
		print(" ============== ")
			
	def generateXY(self,test_cases_count:int,sight_range:int,target_name:str,bonus_sight_range = 0,forced_divisibility_by = 1, data_divider = 1,test_cases_multiplier = 1,ETA_every = 1000, print_example = False, validate = False):
		""" test_cases_count (int) - number of test cases for later analysis
		sight_range (int) - number of candles AI can see in the past
		target_name (str) - name of the column of which value is used to determine Y
		data_divider (int) - reduces data count x-fold, default: 1
		test_cases_multiplier (int) - increases the number of unused values for testing => makes testing more random, default: 1
		ETA_every (int) - print ETA every x ready cases, default: 1000
		print_example (bool) - print first created X and Y value, default: False
		validate (bool) - after printing, wait for user input to continue, default: False
		
		"""
		self.sight_range = sight_range
		self.bonus_sight_range = bonus_sight_range
		self.test_cases_count = test_cases_count
		self.test_cases_multiplier = test_cases_multiplier
		self.target_name = target_name
		self.forced_divisibility_by = forced_divisibility_by
		
		#####
		self.candle_multiplier = 1
		
		self.data_count = self.data_count//data_divider
		self.data_count = self.data_count -(self.sight_range+self.bonus_sight_range)- self.data_count%(sight_range+bonus_sight_range) - test_cases_count*test_cases_multiplier if data_divider == 1 else self.data_count
		self.data_count = self.data_count - self.data_count%forced_divisibility_by
		self.X = np.zeros((self.data_count,sight_range*self.input_size))
		self.Y = np.zeros((self.data_count))
		
		
		print("Creating "+str(self.data_count)+" training cases...")
		time_start = time()
		for i in range(0, self.data_count):
			for s in range(0, sight_range):
				for idx,name in enumerate(self.names):
					self.X[i][s*self.input_size+idx]=self.dataDictionary[name][i+s]
			
			########## THE RULE ##########
			"""
			# 1 - up, 0 - down
			if self.dataDictionary[target_name][i+sight_range] - self.dataDictionary[target_name][i+sight_range-1] > 0: self.Y[i] = 1
			"""
			"""
			# percentage change
			self.percentage_multiplier = 1000
			self.Y[i] = self.percentage_multiplier * (self.dataDictionary[target_name][i+sight_range] - self.dataDictionary[target_name][i+sight_range-1])/self.dataDictionary[target_name][i+sight_range-1]
			if self.Y[i]>1: self.Y[i] = 1
			elif self.Y[i]<-1: self.Y[i] = -1
			"""
			
			
			#Trend guesser
			if abs(self.dataDictionary[target_name][i+sight_range-1+bonus_sight_range] - self.dataDictionary[target_name][i+sight_range-1])>self.candle_multiplier*abs(self.dataDictionary["Open"][i+sight_range-1]-self.dataDictionary["Close"][i+sight_range-1]):
				#if self.dataDictionary[target_name][i+sight_range-1+bonus_sight_range] - self.dataDictionary[target_name][i+sight_range-1]>0:
				if self.dataDictionary["Open"][i+sight_range-1+bonus_sight_range]>self.dataDictionary["Close"][i+sight_range-1+bonus_sight_range] and self.dataDictionary["Open"][i+sight_range-1]>self.dataDictionary["Close"][i+sight_range-1]:
					self.Y[i] = 0
				elif self.dataDictionary["Open"][i+sight_range-1+bonus_sight_range]<self.dataDictionary["Close"][i+sight_range-1+bonus_sight_range] and self.dataDictionary["Open"][i+sight_range-1]<self.dataDictionary["Close"][i+sight_range-1]:
					self.Y[i] = 1
				else:
					self.Y[i] = 0.5
			else:
				self.Y[i] = 0.5
				
			##############################
			if(i%ETA_every == 0 and i!=0):
				print(str(i)+"/"+str(self.data_count),end='')
				print(" ETA: "+str(round((time()-time_start)/i*(self.data_count-i)/60,2))+" min ("+str(round((time()-time_start)/i*(self.data_count-i),2))+" s)               ",end='\r')
			
			if(i==0 and print_example):
				print(self.X[0].reshape((self.sight_range,self.input_size)))
				print(self.Y[0])
				print("X shape: "+str(self.X.shape))
				print("Y shape: "+str(self.Y.shape))
				if(validate):
					if(input("Enter to confirm; anything else to quit\n")!=""):
						exit(0)
			if(validate and not print_example and i==0):
				print("W: Validate is true; but print_example is false. Was this intended?\n")
		print("")	
		print("Counts:")
		print("0: "+str(np.count_nonzero(self.Y==0)))
		print("0.5: "+str(np.count_nonzero(self.Y==0.5)))
		print("1: "+str(np.count_nonzero(self.Y==1)))
		
	def createModel(self,fromModel = None, size_divider = 1,size_multiplier=1):
		size_divider = 2**(size_divider-1)
		if(fromModel!=None and fromModel!=""):
			self.model = tf.keras.models.load_model('bots/'+self.datasheet.replace("_","")+"/"+fromModel+".hdf5")
		else:
			self.model = tf.keras.models.Sequential([

				InputLayer(input_shape=(self.sight_range*self.input_size)),
				#BatchNormalization(),

				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),

				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),

				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),
				
				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),

				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),
				
				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),
				
				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),
				
				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),
				
				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),
				
				Dense(1024//size_divider*size_multiplier, activation="relu"),
				#BatchNormalization(),
				#Dropout(0.05),
				
				Dense(1, activation="sigmoid"),
				])
				
				
		opt = tf.keras.optimizers.Adam(learning_rate=0.000001)
		"""
		# 1 0
		self.model.compile(optimizer='adam', loss=tf.losses.BinaryCrossentropy(), metrics=[tf.keras.metrics.Precision(),'mean_absolute_error'])
		"""
		"""
		#percentage change (-1->1)
		self.model.compile(optimizer='adam', loss=tf.losses.MeanSquaredError(), metrics=[tf.keras.metrics.Accuracy()]) #,'mean_absolute_error'
		"""
		self.model.compile(optimizer=opt, loss=tf.losses.BinaryCrossentropy(), metrics=[tf.keras.metrics.Precision(),'mean_absolute_error'])
		
		self.model.summary()
		
	def train(self,epochs, steps = 0, min_range = 2, max_range = 60, steps_force_custom = False, early_stop = False):
		X = self.X.copy()
		Y = self.Y.copy()
	
		middletargets = 10000
		cc = np.count_nonzero(Y==0.5)
		T = np.zeros((cc-middletargets))
		c = 0
		for i in range(Y.shape[0]):
			if Y[i]==0.5:
				T[c] = i
				c+=1
			if c ==cc-middletargets: break
		X = np.delete(X,T,0)
		Y = np.delete(Y,T,0)
		
		new_len = (Y.shape[0]) - (Y.shape[0])%self.forced_divisibility_by
		Y = Y[:new_len]
		X = X[:new_len]
		
		data_count = Y.shape[0]
		if(steps==0):
			steps = 1
			for i in range(min_range,max_range+1):
				if data_count%i==0: steps = i
		elif(data_count%steps!=0 and not steps_force_custom):
			steps = 1
			for i in range(min_range,max_range+1):
				if data_count%i==0: steps = i
		
		while(data_count%steps!=0):
			print("Incorrect value for steps: "+str(steps)+" and data count: "+str(data_count)+" (remainder is "+str(data_count%steps)+"). Input a new one or 0 for auto")
			steps = input("Value: ")
			if steps==0:
				steps = 1
				for i in range(min_range,max_range+1):
					if data_count%i==0: steps = i
				print("Auto found: "+str(steps)+". Enter to accept")
				if input()!="": steps = data_count+1
		
		print("Epochs: "+str(epochs))
		print("Steps/epoch: "+("FORCED " if steps_force_custom else "")+str(steps)) #Changed, not tested ================================
		print("Total: "+str(epochs*steps))
		sleep(3)
		
		"""
		while(np.count_nonzero(Y==0.5)>3000):
			print(np.count_nonzero(Y==0.5))
			print(Y.shape)
			for i in range(Y.shape[0]):
				if Y[i] == 0.5:
					Y = np.delete(Y,i,0)
					X = np.delete(X,i,0)
					break
		"""
		
		
		
		print("Fixed counts: ")
		print("0: "+str(np.count_nonzero(Y==0)))
		print("0.5: "+str(np.count_nonzero(Y==0.5)))
		print("1: "+str(np.count_nonzero(Y==1)))
		
		
		print(" ====== TRAINING ======== ")
		callback=None
		if early_stop:
			callback = [tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)]
		
		self.model.fit(x=X,y=Y,epochs=epochs,steps_per_epoch=steps,callbacks=callback,shuffle=True)
		
	def test(self,print_tests = False, print_every = 1, columns_count=10, jacked_conversion = 0):
		correct=0
		correct_up = 0
		correct_down = 0
		incorrect_up = 0
		incorrect_down = 0
		errors=[0]*self.test_cases_count
		answers=[0]*self.test_cases_count
		failed_for=[[-69,-69]]*self.test_cases_count
		test_order = sample(range(self.data_count,self.data_count+self.test_cases_count*self.test_cases_multiplier),self.test_cases_count)
		target_name = self.target_name
		sight_range = self.sight_range
		bonus_sight_range = self.bonus_sight_range
		print("Testing...")
		for total,idx in enumerate(test_order):
			x=np.zeros((1,self.sight_range*self.input_size))
			val1 = 0
			val2 = 0
			for s in range (0, self.sight_range+1):
				if s!=self.sight_range:
						for nr,name in enumerate(self.names):
							x[0][s*self.input_size+nr] = self.dataDictionary[name][idx+s]
				"""
				### 1,0,   percent change
				else: val2 = self.dataDictionary[self.target_name][idx+s]
				if s==self.sight_range-1: val1 = self.dataDictionary[self.target_name][idx+s]
				"""
			
			y = self.model.predict(x)
			#y[0][0] = 1/2*math.sin(math.pi*y[0][0]-math.pi/2)+1/2
			if jacked_conversion!=0:
				y[0][0] = 0.5*math.tanh(jacked_conversion*(y[0][0]-0.5))+0.5
			"""
			# 1 up 0 down
			truth = 1 if val2>val1 else 0
			diff = abs(y[0][0]-truth)
			
			"""
			
			"""
			#percent change
			truth =self.percentage_multiplier*(val2-val1)/val1
			if truth>1: truth = 1
			elif truth<-1: truth = -1
			diff = abs(y[0][0]-truth)
			"""
			
			#Trend guesser
			truth = 0.5
			if abs(self.dataDictionary[target_name][idx+sight_range-1+bonus_sight_range] - self.dataDictionary[target_name][idx+sight_range-1])>self.candle_multiplier*abs(self.dataDictionary["Open"][idx+sight_range-1]-self.dataDictionary["Close"][idx+sight_range-1]):
				if self.dataDictionary["Open"][idx+sight_range-1+bonus_sight_range]>self.dataDictionary["Close"][idx+sight_range-1+bonus_sight_range] and self.dataDictionary["Open"][idx+sight_range-1]>self.dataDictionary["Close"][idx+sight_range-1]:
					truth = 0
				elif self.dataDictionary["Open"][idx+sight_range-1+bonus_sight_range]<self.dataDictionary["Close"][idx+sight_range-1+bonus_sight_range] and self.dataDictionary["Open"][idx+sight_range-1]<self.dataDictionary["Close"][idx+sight_range-1]:
					truth = 1
				else:
					truth = 0.5
			diff = abs(y[0][0]-truth)
			
			if(print_tests and total%print_every ==0):
				print(x.reshape((self.sight_range,self.input_size)))
				print(y)
				"""
				# 1 up 0 down
				print("Truth: " + ("up" if truth==1 else "down"))
				print("Up: "+str(round(2*(y[0][0]-0.5),2)) if y[0][0]>0.5 else "Down: "+str(round(2*(abs(0.5-y[0][0])),2)))
				"""
				"""
				# percent change
				print("Truth: " +str(truth)+"% ( multiplied "+str(self.percentage_multiplier)+" times)")
				print("AI: "+str(y[0][0])+"% ---> Err: " +str(diff)+"%")
				"""
				#Trend guesser
				print("Truth: " +str(truth))
				print("AI: "+str(y[0][0])+" ---> Err: " +str(diff))
				
			"""
			# 1 up 0 down
			if(truth==1 and y[0][0]>0.5): correct+=1
			if(truth==0 and y[0][0]<=0.5): correct+=1
			errors[total] = diff
			"""
			"""
			# percent change
			if diff/truth <0.02: correct +=1
			errors[total] = diff
			"""
			
			middle_threshold = 0.5
			correct_threshold = 0.35
			#Trend guesser
			
			if truth==1 and diff<correct_threshold:
				correct_up+=1
				correct+=1
			elif truth==0 and diff<correct_threshold:
				correct_down+=1
				correct+=1
			elif truth==0.5 and diff<middle_threshold:
				correct+=1
			else:
				failed_for[total] = [truth,round(diff,2)]
				if truth==1 and diff>(1-correct_threshold): incorrect_up+=1
				elif truth==0 and diff>(1-correct_threshold): incorrect_down+=1
			errors[total] = diff
			answers[total] = round(y[0][0],2)
			
		errors.sort()
		answers.sort()
		#failed_for.sort()
		failed_for = sorted(failed_for,key=lambda x: x[0])
		for f in range(1,len(failed_for)):
			if failed_for[f][0] != -69 and failed_for[f-1][0] ==-69:
				failed_for = failed_for[f:]
				break
		if failed_for[0][0]==-69:
			failed_for = []
				
		l = len(answers)
		shaper = columns_count
		answers = np.array(answers)
		print("=============================================")
		print("RESULT: "+str(correct)+"/"+str(self.test_cases_count)+" => "+str(round(correct/self.test_cases_count*100,2))+"%")
		print("Avg err: "+str(sum(errors)/len(errors))+", median: "+str(errors[len(errors)//2])+", max: "+str(errors[-1])+", min: "+str(errors[0]))
		print("Guesses: ")
		print(answers.reshape(l//shaper,shaper))
		print("Failed on: ")
		print(failed_for)
		if correct_down == 0 and incorrect_down == 0: correct_down = 1
		if correct_up == 0 and incorrect_up == 0: correct_up = 1
		print("Correct/Incorrect UP")
		print(str(correct_up)+":"+str(incorrect_up)+" ==> "+str(correct_up/(incorrect_up+correct_up)*100)+"%")
		print("Correct/Incorrect DOWN")
		print(str(correct_down)+":"+str(incorrect_down)+" ==> "+str(correct_down/(incorrect_down+correct_down)*100)+"%")
		print("Correct/Incorrect TOTAL")
		print(str(correct_up+correct_down)+":"+str(incorrect_up+incorrect_down)+" ==> "+str((correct_down+correct_up)/(incorrect_up+correct_up+incorrect_down+correct_down)*100)+"%")
		self.correct = correct
		
	def save(self):
		if not os.path.exists('bots/'+self.datasheet.replace("_","")):
			os.makedirs('bots/'+datasheet.replace("_",""))
		
		name = "forexmodel-"+str(round(self.correct/self.test_cases_count*100,2))+"%-"+str(datetime.date(datetime.now()))+"-"+str(time())
		self.model.save("bots/"+self.datasheet.replace("_","")+"/"+name+".hdf5")
		print("Saved as: "+name)
