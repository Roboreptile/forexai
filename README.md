# ForexAI - Deep Neural Network predicting forex price movements

Contains data for EURUSD and EURPLN forex pairs.

## Project Description

Use the pairs on most common time windows (H1, H4, D1) and train a DNN to predict the future stock movements.

* [X] Create a basic model
* [X] Implement methods to easily modify and test models
* [X] Achieve good results - test on real-time data for a month
* [ ] Improve results [WIP]

## Before Running:

In the command line, navigate to the folder directory and run (preferably on a fresh python environment):
Windows:
`python -m pip install -r requirements.txt`
Linux/Mac:
`python3 -m pip install -r requirements.txt`

## Solution Description

*main.py* contains a Robotics class wich describes the input/output preparation, training duration, and special parameters such as sight range (how far the DNN can see back in time) and selected target predicted value ("Open", "Close", ect). The script also allows to load a trained model and test it on a given XY-dataset.

## Results of one month test

Tested on H1 interval for EURUSD pair for one month with 100 000 starting money, using TradingView platform

![](demos/test.png)

0.1% Profit mostly due to one incorrect trade, without it the profit would reach around 1%.
